# OH_RISC-V_SIG

# 介绍
RISC-V SIG 组旨在构建围绕OpenHarmony的软硬件生态，提供RISC-V的软件包和系统构建等指导，并维护对RISC-V设备的支持，使能RISC-V场景下的应用、安全等能力。 

RISC-V SIG将推动对 RISC-V OpenHarmony感兴趣的开发者能够参与到开源系统开发中活动中来。

# 工作目标和范围
 * 为OpenHarmony维护RISC-V发行版

   SIG将维护OpenHarmony的RISC-V发行版，建设RISC-V适配软件包源

 * OpenHarmony在RISC-V体系结构和设备的适配

   SIG将不断更新（移植）和维护OpenHarmony能够支持的RISC-V设备。

 * RISC-V 安全能力

   SIG将结合蓬莱RISC-V TEE使能OpenHarmony在RISC-V场景下的安全能力。

* 生态扩展

​       积极与开发者、高校和相关客户合作，推动OpenHarmony在RISC-V环境下的应用生态建设

# 组织会议

* 目前视任务进度情况决定开会时间
* 通过邮件申报议题(邮件列表暂未完善，请先发送至jiageng08@iscas.ac.cn)

# 成员

## Maintainer列表

* jiageng08@iscas.ac.cn

联系方式

* 邮件列表: 暂无

